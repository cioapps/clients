package org.fao.sola.admin.web.beans.helpers;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Used to display messages
 */
@Named
@RequestScoped
public class MessageBean implements Serializable {
    private static final long serialVersionUID = 1905122041950251207L;
    
    public enum MESSAGE_TYPE{ERROR, WARNING, SUCCESS}

    private String message = "";
    private MESSAGE_TYPE type = MESSAGE_TYPE.SUCCESS;

    public String getMessage() {
        return message;
    }
    
    public boolean isError(){
        return type == MESSAGE_TYPE.ERROR;
    }

    public boolean isSuccess(){
        return type == MESSAGE_TYPE.SUCCESS;
    }
    
    public boolean isWarning(){
        return type == MESSAGE_TYPE.WARNING;
    }
    
    public void setMessage(String message, MESSAGE_TYPE type) {
        this.message = message;
        this.type = type;
    }
    
    public void setSuccessMessage(String message) {
        setMessage(message, MESSAGE_TYPE.SUCCESS);
    }
    
    public void setErrorMessage(String message) {
        setMessage(message, MESSAGE_TYPE.ERROR);
    }
    
    public void setWarningMessage(String message) {
        setMessage(message, MESSAGE_TYPE.WARNING);
    }
}
